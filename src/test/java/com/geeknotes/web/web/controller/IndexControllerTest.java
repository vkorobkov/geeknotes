package com.geeknotes.web.web.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.util.reflection.Whitebox;
import org.springframework.web.servlet.ModelAndView;

import static org.junit.Assert.assertEquals;

public class IndexControllerTest {

    private static final String BRAND = "GeekNotes";

    private IndexController indexController;

    @Before
    public void setup() {
        indexController = new IndexController();
        Whitebox.setInternalState(indexController, "productBrand", BRAND);
    }

    @Test
    public void testIndexDoesRedirectToEditor() {
        assertEquals("redirect:/editor/", indexController.index());
    }

    @Test
    public void editorPositiveTest() {
        ModelAndView mav = indexController.editor();

        assertEquals("editor", mav.getViewName());
        assertEquals(BRAND, mav.getModel().get("product_brand"));
    }
}