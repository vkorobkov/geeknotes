package com.geeknotes.web.web.config;

import org.junit.Test;
import org.mockito.internal.util.reflection.Whitebox;
import org.springframework.web.filter.CharacterEncodingFilter;

import static org.junit.Assert.assertEquals;

public class CommonConfigTest {

    @Test
    public void testCharacterEncodingFilter() throws Exception {
        CharacterEncodingFilter filter = new CommonConfig().characterEncodingFilter();

        assertEquals("UTF-8", Whitebox.getInternalState(filter, "encoding"));
        assertEquals(true, Whitebox.getInternalState(filter, "forceEncoding"));
    }
}