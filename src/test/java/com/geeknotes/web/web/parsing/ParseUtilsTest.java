package com.geeknotes.web.web.parsing;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ParseUtilsTest {

    @Test(expected = NullPointerException.class)
    public void shouldRemoveExtraSpacesThrowNPEIfArgumentIsNull() {
        ParseUtils.removeExtraSpaces(null);
    }

    @Test
    public void shouldRemoveExtraSpacesDoNothingWithEmptyString() {
        assertEquals("", ParseUtils.removeExtraSpaces(""));
    }

    @Test
    public void shouldRemoveExtraSpacesWorkInTheMiddleOfTheString() {
        assertEquals("Vladimir Vadimovich Korobkov", ParseUtils.removeExtraSpaces("Vladimir     Vadimovich      Korobkov"));
    }

    @Test
    public void shouldRemoveExtraSpacesTrimAllTheSpacesAtTheBeginningAndEnding() {
        assertEquals("Vovan", ParseUtils.removeExtraSpaces("  Vovan    "));
    }

    @Test(expected = NullPointerException.class)
    public void shouldRemoveNewLinesSymbolsThrowNPEIfArgumentIsNull() {
        ParseUtils.replaceNewLineSymbolsWithSpaces(null);
    }

    @Test
    public void shouldRemoveNewLinesSymbolsWithSpacesPositiveTest() {
        assertEquals("  Hello World ", ParseUtils.replaceNewLineSymbolsWithSpaces("\n\nHello\nWorld\n"));
    }

    @Test
    public void shouldExtractTagsPositiveTest() {
        List<String> tags = ParseUtils.extractTags("/tag1 /tag2");

        assertTrue(tags.contains("tag1"));
        assertTrue(tags.contains("tag2"));
    }

    @Test
    public void shouldExtractTagsKeepCaseSense() {
        List<String> tags = ParseUtils.extractTags("/tAg1 /tAg2");

        assertTrue(tags.contains("tAg1"));
        assertTrue(tags.contains("tAg2"));
    }

    @Test
    public void shouldExtractTagsFromTheText() {
        List<String> tags = ParseUtils.extractTags("Hello /tag1 myster /tag2 Smith!");

        assertTrue(tags.contains("tag1"));
        assertTrue(tags.contains("tag2"));
    }

    @Test
    public void shouldExtractTagsFromTheTextAndMultipleLines() {
        List<String> tags = ParseUtils.extractTags("\nHello,\n /tag1 myster /tag2 Smith!\nHow r u? /tag3");

        assertTrue(tags.contains("tag1"));
        assertTrue(tags.contains("tag2"));
        assertTrue(tags.contains("tag3"));
    }

    @Test
    public void cropStringTest() {
        assertEquals("Hel", ParseUtils.cropString("Hello, World", 3));
        assertEquals("Hello, World", ParseUtils.cropString("Hello, World", 100));
        assertEquals("", ParseUtils.cropString("Hello, World", 0));
    }

    @Test
    public void shouldCutTagsEasyTest() {
        assertEquals("Hello, world!", ParseUtils.cutTagsFromPayload("Hello, /tag world!"));
    }

    @Test
    public void shouldCutTagsFromMultilineTextTest() {
        assertEquals("Hello,\n\nworld!", ParseUtils.cutTagsFromPayload("Hello,\n/tag\nworld!"));
    }
}