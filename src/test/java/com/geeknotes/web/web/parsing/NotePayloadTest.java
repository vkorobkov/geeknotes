package com.geeknotes.web.web.parsing;

import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class NotePayloadTest {

    public static final String HTML = "html";
    public static final String BODY = "body";
    public static final String HEADER = "header";

    @Test
    public void shouldHasTagsReturnTrueIfThereIsATag() {
        NotePayload payload = new NotePayload(HTML, BODY, HEADER, Collections.singletonList("tag"));
        assertTrue(payload.hasTags());
    }

    @Test
    public void shouldHasTagsReturnFalseIfThereIsNoTags() {
        NotePayload payload = new NotePayload(HTML, BODY, HEADER, Collections.emptyList());
        assertFalse(payload.hasTags());
    }

    @Test
    public void shouldIsEmptyReturnTrueIfBodyAndHeaderAndTagsAreEmpty() {
        NotePayload payload = new NotePayload(HTML, "", "", Collections.emptyList());
        assertTrue(payload.isEmpty());
    }
}