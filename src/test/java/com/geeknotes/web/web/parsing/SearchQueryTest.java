package com.geeknotes.web.web.parsing;

import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SearchQueryTest {

    @Test
    public void shouldHasTagsBeTrueIfThereIsTag() {
        assertTrue(new SearchQuery(Collections.singletonList("tag"), "query").hasTags());
    }

    @Test
    public void shouldHasTagsBeFalseIfThereAreNoTags() {
        assertFalse(new SearchQuery(Collections.emptyList(), "query").hasTags());
    }

    @Test
    public void shouldHasQueryBeTrueIfThereIsQuery() {
        assertTrue(new SearchQuery(Collections.emptyList(), "query").hasQuery());
    }

    @Test
    public void shouldHasQueryBeFalseIfThereIsNoQuery() {
        assertFalse(new SearchQuery(Collections.emptyList(), "").hasQuery());
    }
}