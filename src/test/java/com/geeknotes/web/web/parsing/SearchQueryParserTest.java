package com.geeknotes.web.web.parsing;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SearchQueryParserTest {

    private final SearchQueryParser parser = new SearchQueryParser();

    @Test
    public void positiveTest() {
        SearchQuery query = parser.parse("/home /internet bill");

        assertEquals("bill", query.getQuery());
        assertTrue(query.getTags().contains("home"));
        assertTrue(query.getTags().contains("internet"));
    }

    @Test
    public void shouldCropExtraSpaces() {
        SearchQuery query = parser.parse("    /home   /internet  bill    ");

        assertEquals("bill", query.getQuery());
        assertTrue(query.getTags().contains("home"));
        assertTrue(query.getTags().contains("internet"));
    }

    @Test
    public void shouldReturnEmptyListIfThereIsNoTags() {
        SearchQuery query = parser.parse("bill");

        assertEquals("bill", query.getQuery());
        assertTrue(query.getTags().isEmpty());
    }

    @Test
    public void shouldReturnEmptyListAndBlankStringIfQueryIsEmpty() {
        SearchQuery query = parser.parse("");

        assertEquals("", query.getQuery());
        assertTrue(query.getTags().isEmpty());
    }
}