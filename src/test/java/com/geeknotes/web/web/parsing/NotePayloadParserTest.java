package com.geeknotes.web.web.parsing;

import org.junit.Test;

import static org.junit.Assert.*;

public class NotePayloadParserTest {

    private final NotePayloadParser parser = new NotePayloadParser();

    @Test
    public void shouldExtractHeaderAsH1() {
        NotePayload payload = parser.parse("# Header\n" +
                "Hello");

        assertEquals("Header", payload.getHeader());
    }

    @Test
    public void shouldExtractHeaderAsAText() {
        NotePayload payload = parser.parse("No MD Header\n" +
                "Hello");

        assertEquals("No MD Header Hello", payload.getHeader());
    }

    @Test
    public void shouldExtractHeaderFromHtmlTag() {
        NotePayload payload = parser.parse("<b>No MD Header</b>\n" +
                "Hello");

        assertEquals("No MD Header Hello", payload.getHeader());
    }

    @Test
    public void shouldReturnEmpyTagListIfNoTags() {
        NotePayload payload = parser.parse("<b>No MD Header</b>");

        assertFalse(payload.hasTags());
        assertTrue(payload.getTags().isEmpty());
    }

    @Test
    public void shouldReturnTagListIfThereAreTags() {
        NotePayload payload = parser.parse("<b>No /tag MD Header</b>");

        assertTrue(payload.hasTags());
        assertTrue(payload.getTags().contains("tag"));
    }
}