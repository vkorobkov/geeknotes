package com.geeknotes.web.service;


import com.geeknotes.web.model.sql.User;
import com.geeknotes.web.model.sql.VipUserCredentials;
import com.geeknotes.web.repo.sql.VipUserCredentialsRepository;
import com.geeknotes.web.util.SecuredUser;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

public class UserDetailsLookupServiceTest {

    @InjectMocks
    private UserDetailsLookupService userDetailsLookupService;

    @Mock
    private VipUserCredentialsRepository vipUserCredentialsRepository;

    @Mock
    private VipUserCredentials vipUserCredentials;

    @Mock
    private User user;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = UsernameNotFoundException.class)
    public void shouldThrowExceptionIfUserWasNotFoundByName() {
        // Given
        when(vipUserCredentialsRepository.findByName(anyString())).thenReturn(null);

        // When
        userDetailsLookupService.loadUserByUsername("John Connor");
    }

    @Test
    public void shouldReturnSecuredUserTest() {
        // Some constants
        final String USER_NAME = "vovan";
        final String PASSWORD = "password";
        final Long USER_ID = 5L;
        final String ROLES = "ROLE_ADMIN,ROLE_USER";

        // Given
        when(vipUserCredentialsRepository.findByName(USER_NAME)).thenReturn(vipUserCredentials);
        when(vipUserCredentials.getUser()).thenReturn(user);
        when(vipUserCredentials.getPassword()).thenReturn(PASSWORD);
        when(user.getId()).thenReturn(USER_ID);
        when(user.getRole()).thenReturn(ROLES);

        //When
        SecuredUser result = (SecuredUser) userDetailsLookupService.loadUserByUsername(USER_NAME);

        // Then
        assertEquals(PASSWORD, result.getPassword());
        assertEquals(USER_ID, result.getUserId());
        assertEquals(USER_NAME, result.getUsername());
        assertEquals(2, result.getAuthorities().size());
    }
}