package com.geeknotes.web.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class EasyStopWatchTest {

    @Test
    public void shouldReturnZeroOrPositiveElapsedTime() {
        // Given
        EasyStopWatch stopWatch = new EasyStopWatch().start();

        // When
        stopWatch.stop();

        // Then
        assertTrue(stopWatch.totalMilliSeconds() >= 0);
    }

    @Test
    public void shouldNotDoubleStartThrowAnException() {
        new EasyStopWatch().start().start();
    }

    @Test
    public void shouldStartMethodReturnInstanceOfTheClass() {
        // Given
        EasyStopWatch stopWatch = new EasyStopWatch();

        // When
        EasyStopWatch startedStopWatch = stopWatch.start();

        // Then
        assertSame(stopWatch, startedStopWatch);
    }

    @Test
    public void shouldNotDoubleStopThrowAnException() {
        new EasyStopWatch().stop().stop();
    }

    @Test
    public void shouldStopMethodReturnInstanceOfTheClass() {
        // Given
        EasyStopWatch stopWatch = new EasyStopWatch();

        // When
        EasyStopWatch startedStopWatch = stopWatch.stop();

        // Then
        assertSame(stopWatch, startedStopWatch);
    }

    @Test
    public void shouldNotInitializedStopWatchShowZero() {
        assertEquals("0 millisecond(s)", new EasyStopWatch().toString());
    }
}