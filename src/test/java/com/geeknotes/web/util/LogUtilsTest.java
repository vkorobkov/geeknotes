package com.geeknotes.web.util;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;

import static org.mockito.Mockito.*;

public class LogUtilsTest {

    private final static String DEBUG_MESSAGE = "Some message";

    @Mock
    private Logger logger;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldLogIfDebugIsOn() {
        // Given
        when(logger.isDebugEnabled()).thenReturn(true);

        // When
        LogUtils.debugIfEnabled(logger, () -> DEBUG_MESSAGE);

        // Then
        verify(logger, times(1)).debug(DEBUG_MESSAGE);
    }

    @Test
    public void shouldNotLogIfDebugIsOff() {
        // Given
        when(logger.isDebugEnabled()).thenReturn(false);

        // When
        LogUtils.debugIfEnabled(logger, () -> DEBUG_MESSAGE);

        // Then
        verify(logger, times(0)).debug(DEBUG_MESSAGE);
    }
}