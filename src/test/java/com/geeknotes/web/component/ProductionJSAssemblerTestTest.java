package com.geeknotes.web.component;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertTrue;

public class ProductionJSAssemblerTestTest {

    private ProductionJSAssembler assembler;

    @Before
    public void setup() {
        assembler = new ProductionJSAssembler();
    }

    @After
    public void tearDown() {
        ReflectionTestUtils.invokeMethod(assembler, "cleanupTempFolder");
    }

    @Test
    public void shouldGlueTheFilesIntoOne() {
        // When
        ReflectionTestUtils.invokeMethod(assembler, "setup");

        // Then
        URI fileUri = assembler.getProductionFolderURI();
        assertTrue(Files.exists(Paths.get(fileUri)));
    }

}