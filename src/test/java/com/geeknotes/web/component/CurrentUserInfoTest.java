package com.geeknotes.web.component;

import com.geeknotes.web.model.sql.User;
import com.geeknotes.web.repo.sql.UserRepository;
import com.geeknotes.web.util.SecuredUser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.when;

public class CurrentUserInfoTest {

    private final static Long USER_ID = 1L;

    private final static SecuredUser SECURITY_USER = new SecuredUser(USER_ID, "user_name", "pass", Collections.emptyList());

    @InjectMocks
    private CurrentUserInfo currentUserInfo;

    @Mock
    private SecurityContext securityContext;

    @Mock
    private Authentication authentication;

    @Mock
    private UserRepository userRepository;

    @Mock
    private User user;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.getPrincipal()).thenReturn(SECURITY_USER);
        when(userRepository.findOne(USER_ID)).thenReturn(user);

        SecurityContextHolder.setContext(securityContext);
    }

    @After
    public void tearDown() {
        SecurityContextHolder.clearContext();
    }

    @Test
    public void shouldReturnUserTest() {
        assertSame(user, currentUserInfo.getUser());
    }

    @Test
    public void shouldReturnUserIdTest() {
        // Given
        final Long EXPECTED_USER_ID = 5L;
        when(user.getId()).thenReturn(EXPECTED_USER_ID);

        // Then
        assertEquals(EXPECTED_USER_ID,  currentUserInfo.getUserId());
    }
}