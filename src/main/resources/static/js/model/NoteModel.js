var app = app || {};


$(function($) {

    app.NoteModel = Backbone.Model.extend({
        urlRoot: '/editor/notes/note/',
        isSaving: false,
        editorEventData: null,
        onStartAjaxCallback: null,
        onLimitExceededCallback: null,
        windowHashChangeActor: null,
        BODY_MAX_LENGTH: 65535,
        //BODY_MAX_LENGTH: 5,

        defaults: {
            defaultBody:    '# New note\n\n' +
                            'Enter your markdown here and see the preview on the right panel'
        },

        initialize: function() {
            this.saveScheduleFunc = app.Utils.createTimeOutTriggerFunction(3000, this.saveNote, this);

            // Global events
            app.Events.View.Editor.Change.on(this.notifyEditorWasUpdatedByUser, this);

            // Custom events
            this.on('change:id', this.onIdChange);
        },

        notifyEditorWasUpdatedByUser: function(editorEventData) {
            this.editorEventData = editorEventData;
            this.saveScheduleFunc();
        },

        onIdChange: function(model, id) {
            var afterNoteGotSaved = function () {
                if (this.isNew()) {
                    this.set('body', this.attributes.defaultBody);
                    this.windowHashChangeActor('#');
                }
                else {
                    this.loadNote();
                    this.windowHashChangeActor('#' + this.id);
                }
            }.bind(this);
            this.saveNote(afterNoteGotSaved);
        },

        loadNote: function() {
            // id is updating when new note has saved for the first time - in this case don't load from the server
            if (this.isSaving) {
                return;
            }

            console.log("Loading note with Id=" + this.id);

            this.triggerAjaxCallback();
            this.fetch({ wait: true});

            console.log("Note loaded")
        },

        saveNote: function(doneHandler) {
            console.log("Saving note");

            // What to do if thie saving was done
            var doneCallback = function() {
                this.isSaving = false;
                if (typeof(doneHandler) == 'function') {
                    doneHandler();
                }
            }.bind(this);

            // No editor yet
            if (this.editorEventData === null) {
                doneCallback();
                return;
            }

            // No changes - not saving
            if ($.trim(this.attributes.body) === $.trim(this.editorEventData.getValue())) {
                console.log('There is no changes to save. Exiting.');
                doneCallback();
                return;
            }

            // Set body and firing event
            this.setBody(this.editorEventData.getValue());
            this.triggerAjaxCallback();
            this.isSaving = true;

            // Running ajax call and handling the results
            this.save({}, { context: this }).always(doneCallback);

            console.log('Note saved');
        },

        setBody: function(newBody) {
            if (newBody.length > this.BODY_MAX_LENGTH) {
                newBody = newBody.substr(0, this.BODY_MAX_LENGTH);
                this.onLimitExceededCallback();
            }
            this.set('body', newBody, {silent: true});
        },

        triggerAjaxCallback: function() {
            if (this.onStartAjaxCallback) this.onStartAjaxCallback();
        }
    });

});