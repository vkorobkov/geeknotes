
$(function($) {

    app.SearchModel = Backbone.Model.extend({
        QUERY_ATTRIBUTE: 'query',
        CONTENT_ATTRIBUTE: 'content',
        PAGE_INFO_ATTRIBUTE: 'pageInfo',
        LOADING_ATTRIBUTE: 'loading',
        onOpenDocumentCallback: null,

        attributes: {
            query: '',
            selectedIndex: null,
            content: null,
            pageInfo: null,
            serverError: false,
            loading: false
        },

        urlRoot: function() {
            return '/editor/notes/search/?page=0&query=' + this.attributes.query;
        },

        initialize: function() {
            this.on('change:' + this.QUERY_ATTRIBUTE, this.onQueryChange, this);
        },

        onSearchByTimer: function (query) {
            this.setQuery(query);
        },

        onEnterClick: function(query) {
            if (this.get(this.QUERY_ATTRIBUTE) === query) {
                var selectedIndex = this.attributes.selectedIndex;
                if (selectedIndex == null) {
                    return;
                }
                var idToOpen = this.attributes.content[selectedIndex].id;
                this.onOpenDocumentCallback(idToOpen);
            }
            else {
                this.setQuery(query);
            }
        },

        onQueryChange: function () {
            this.setSelectedIndex(null);
            this.setServerError(false);

            if (this.hasQueryString()) {
                this.performSearch();
            }
        },

        performSearch: function() {
            this.setIsLoading(true);

            var result = this.fetch({ context: this });

            result.always(function() {
                this.setIsLoading(false);
            });
            result.done(this.moveSelectedDown);
            result.fail(function() {
                this.setServerError(true);
                this.set(this.CONTENT_ATTRIBUTE, null);
                this.set(this.PAGE_INFO_ATTRIBUTE, null);
            });
        },

        hasQueryString: function() {
            return this.get(this.QUERY_ATTRIBUTE) ? true : false;
        },

        hasSearchResults: function() {
            return this.has(this.CONTENT_ATTRIBUTE) && this.has(this.PAGE_INFO_ATTRIBUTE) && this.attributes.content.length > 0;
        },

        abbreviate: function() {
            var LEN = 100;
            return function(text, render) {
                text = render(text);
                return text.length>LEN ? text.substr(0, LEN - 3) + '...' : text;
            }
        },

        moveSelectedDown: function() {
            if (!this.hasSearchResults()) {
                return;
            }
            this.incrementSelectedIndex();
        },

        moveSelectedUp: function() {
            if (!this.hasSearchResults()) {
                return;
            }
            this.decrementSelectedIndex();
        },

        incrementSelectedIndex: function() {
            var length = this.attributes.content.length;
            var selectedIndex = this.attributes.selectedIndex;
            if (selectedIndex !== null && selectedIndex < (length - 1)) {
                selectedIndex++;
            }
            else {
                selectedIndex = 0;
            }
            this.updateSelectedItemStyle(selectedIndex);
            this.setSelectedIndex(selectedIndex);
        },

        decrementSelectedIndex: function() {
            var selectedIndex = this.attributes.selectedIndex;
            if (selectedIndex > 0) {
                selectedIndex--;
            }
            else {
                selectedIndex = this.attributes.content.length - 1;
            }
            this.updateSelectedItemStyle(selectedIndex);
            this.setSelectedIndex(selectedIndex);
        },


        updateSelectedItemStyle: function(selectedIndex) {
            var content = this.attributes.content;
            var length =  this.attributes.content.length;
            for (var j = 0; j < length; j++) {
                if (j == selectedIndex) {
                    content[j].selected = 'search-result-item-selected';
                }
                else {
                    content[j].selected = '';
                }
            }
        },

        setQuery: function(query) {
            this.set(this.QUERY_ATTRIBUTE, query);
        },

        setServerError: function(error) {
            this.set('serverError', error);
        },

        setIsLoading: function(loading) {
            this.set(this.LOADING_ATTRIBUTE, loading);
        },

        setSelectedIndex: function(selectedIndex) {
            this.set('selectedIndex', selectedIndex);
        }
    });
});