var app = app || {};

$(function() {
    new app.Preview();
    new app.EditorFooterBar();
    new app.Editor();
    new app.UpperMainMenu();
    new app.LimitExceededWarningView();

    app.noteModel = new app.NoteModel();
    app.searchModel = new app.SearchModel();

    app.Events.Global.ApplicationStarted.trigger();

    // Delete splash screen
    $('#initial-loading-image').remove();
});