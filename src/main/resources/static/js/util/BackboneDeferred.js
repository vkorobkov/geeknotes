'use strict';

$(function($) {

    var origSync = Backbone.sync;

    Backbone.sync = function (method, model, options) {

        // Keep track of original callbacks to have a seamless integration
        var origSuccess = options.success;
        var origError = options.error;
        var context = options.context;

        // Let's get a nice promise
        var deferred = $.Deferred();

        // Override success
        options.success = function (model, response, options) {
            if (origSuccess) origSuccess(model, response, options);
            if (context) {
                return deferred.resolveWith(context, model);
            }
            else {
                return deferred.resolve(model);
            }
        };
        // Override error
        options.error = function (model, response, options) {
            if (origError) origError(model, response, options);
            if (context) {
                return deferred.rejectWith(context, response);
            }
            else {
                return deferred.reject(response);
            }
        };

        origSync(method, model, options);

        return deferred.promise();
    }
});