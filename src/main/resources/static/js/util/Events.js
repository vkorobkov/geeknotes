var app = app || {};

$(function() {

    app.Events = {

        // Global events for all application layers
        Global: {
            Hotkeys: {
                Escape: {}
            },

            ApplicationStarted: {}
        },

        Model: {
            Note: {
                AjaxHasStarted: {}
            }
        },

        View: {
            Editor: {
                Change: {},
                ChangeCursor: {}
            }
        }
    };

    // Standard backbone event bus
    app.dispatcher = _.clone(Backbone.Events);

    // Perform the initialization of events
    function decorateEventBusFunctions(obj) {
        var eventName = obj._name;
        var eventBus = app.dispatcher;
        obj.on      = eventBus.on.bind(eventBus, eventName);
        obj.once    = eventBus.once.bind(eventBus, eventName);
        obj.off     = eventBus.off.bind(eventBus, eventName);
        obj.trigger = eventBus.trigger.bind(eventBus, eventName);
        obj.stopListening = function(obj, name, callback) {
            eventBus.stopListening(obj, eventName, callback).bind(eventBus);
        }
    };

    function makeNames(parentObj) {
        Object.keys(parentObj).forEach(function(key) {
            if (key == '_name') {
                return;
            }

            var obj = parentObj[key];
            var hasKeys = Object.keys(obj).length > 0;
            obj._name = parentObj._name + '.' + key;
            if (hasKeys) {
                makeNames(obj);
            }
            else {
                decorateEventBusFunctions(obj);
            }
        });
    };

    app.Events._name = 'app.Events';
    makeNames(app.Events);

    console.log('Evens object has been initialized');
});