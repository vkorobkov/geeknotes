var app = app || {};

$(function() {

    app.Utils = {
        loadedTemplates: {},

        loadTemplate: function(name, callback, context) {
            callback = (typeof context !== 'undefined') ? callback.bind(context) : callback;
            var cachedTemplate = app.Utils.loadedTemplates[name];
            if (cachedTemplate) {
                return callback(cachedTemplate);
            }
            else {
                $.get(name, function(template) {
                    app.Utils.loadedTemplates[name] = template;
                    return callback(template);
                });
            }
        },

        "uuid": function() {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
                return v.toString(16);
            });
        },

        createEventBroadCasterFor: function(forObj, prefix) {
            return function(onEvent, toEvent) {
                forObj.on(onEvent, function() {
                    toEvent = (typeof toEvent !== 'undefined') ? toEvent : onEvent;
                    var evName = prefix + ":" + toEvent;
                    app.dispatcher.trigger.bind(app.dispatcher, evName).apply(app.dispatcher, arguments);
                }, forObj);
            }
        },

        createTimeOutTriggerFunction: function(delay, callback, context) {
            var timerId = null;
            return function() {
                // clearing perv timers
                if (timerId != null) {
                    clearTimeout(timerId);
                    timerId = null;
                }

                // binding stuff
                var boundCallback = callback.bind(context);
                for(var i = 0; i < arguments.length; i++) {
                    boundCallback = boundCallback.bind(context, arguments[i]);
                }

                timerId = setTimeout(boundCallback, delay);
            }
        },

        askYesNo: function(options) {
            app.Utils.loadTemplate('/static/templates/confirm-box.html', function(template) {
                var rendered = Mustache.render(template, options);
                $('body').append(rendered);
                var el = $('#confirm-box');

                el.modal({backdrop: 'static'});

                var isYes = false;
                $('button[data-item-yes]', el).click(function() {
                    isYes = true;
                    el.modal('hide');
                    el.remove();
                });

                el.on('hide.bs.modal', function() {
                    if (isYes) {
                        if (options.onYes) options.onYes();
                    }
                    else {
                        if (options.onNo) options.onNo();
                    }
                });
            });
        }
    };
});