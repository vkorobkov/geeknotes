var app = app || {};


$(function($) {

    app.EditorEventData = function(editor) {

        var editor = editor;

        this.getValue = function() {
            if (!this.editorValue) {
                this.editorValue = editor.getValue();
            }
            return this.editorValue;
        };

        this.getCursor = function() {
            return editor.getSelection().getCursor();
        }
    };
});