var app = app || {};

$(function($) {

    app.SearchPopupView = Backbone.View.extend({

        initialize: function() {
            // Loading template(or taking from cache), adding to DOM and showing popup
            app.Utils.loadTemplate('/static/templates/search-popup.html', function(template) {
                $('body').append(template);
                this.setElement('#search-box-modal');
                this.initModalWindow();
            }, this);
        },

        initModalWindow: function() {
            this.$el.modal({backdrop: 'static'});

            // Hide modal on escape
            app.Events.Global.Hotkeys.Escape.once(this.hideWindow, this);

            // Unbind events and remove from DOM when modal is hided
            this.$el.on('hide.bs.modal', function() {
                this.searchBox.unbind();
                this.$el.unbind();
                this.unbindSearchResultHandlers();
                this.remove();
            }.bind(this));

            // jQuery stuff caching
            this.searchBox = $('#search-box-modal input');
            this.searchResults = $('#search-box-modal-results');

            // Set up the model
            this.setupModel();

            // Initial rendering
            this.render();
        },

        setupModel: function() {
            // Do render on network events
            app.searchModel.on('sync', this.render, this);
            app.searchModel.on('error', this.render, this);

            // Dp render on changing attributes
            app.searchModel.on('change', this.render, this);

            // Open document callback - just to change {url}/#id and close the dialog
            app.searchModel.onOpenDocumentCallback = this.openNote.bind(this);

            // Arrow up and down to choose the result
            this.searchBox.bind('keydown', 'down', function(e) {
                app.searchModel.moveSelectedDown();
                e.preventDefault();
            });
            this.searchBox.bind('keydown', 'up', function(e) {
                app.searchModel.moveSelectedUp();
                e.preventDefault();
            });

            // Perform search on enter
            this.searchBox.bind('keydown', 'return', function() {
                app.searchModel.onEnterClick(this.getQuery());
            }.bind(this));

            // Timer for auto search by timer
            var triggerFunc = app.Utils.createTimeOutTriggerFunction(500, function() {
                app.searchModel.onSearchByTimer(this.getQuery());
            }, this);
            this.searchBox.on('propertychange change keyup paste input', triggerFunc);
        },

        openNote: function(id) {
            window.location.hash = '#' + id;
            this.hideWindow();
        },

        render: function() {
            // No model - no render
            if (!app.searchModel) {
                return;
            }

            this.renderSearchBox();

            this.renderSearchResults();
            this.searchResultsLinks = $('#search-box-modal-results a');
            this.rebindResultClickHandlers();

            return this;
        },

        renderSearchBox: function() {
            this.searchBox.val(app.searchModel.attributes.query);
            if (app.searchModel.has(app.searchModel.LOADING_ATTRIBUTE)) {
                this.searchBox.prop('disabled', app.searchModel.attributes.loading);
            }
            this.searchBox.focus();
        },

        renderSearchResults: function() {
            app.Utils.loadTemplate('/static/templates/search-popup-results.html', function(template) {
                var rendered = Mustache.render(template, app.searchModel);
                this.searchResults.html(rendered);
            }, this);
        },

        rebindResultClickHandlers: function() {
            this.unbindSearchResultHandlers();
            this.searchResultsLinks.bind('click', function () {
                this.hideWindow();
            }.bind(this));
        },

        getQuery: function() {
            return $.trim(this.searchBox.val());
        },

        unbindSearchResultHandlers: function() {
            this.searchResultsLinks.unbind();
        },

        hideWindow: function() {
            this.$el.modal('hide');
        }
    });
});