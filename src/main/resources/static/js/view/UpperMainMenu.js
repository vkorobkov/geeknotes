var app = app || {};

$(function($) {

    app.UpperMainMenu = Backbone.View.extend({
        el: '#upper-main-menu',

        events: {
            'click #main-menu-search-button': 'handleSearch'
        },

        initialize: function() {
            this.initHotKeys();
        },

        initHotKeys: function() {
            this.bindHotKey('alt+insert', this.handleNewNote);
            this.bindHotKey('alt+n', this.handleSearch);
            this.bindHotKey('esc', this.broadCastEscapeHandler);
        },

        bindHotKey: function(keys, handler) {
            $(document).bind('keydown', keys, handler);
            $('textarea[class="ace_text-input"]').bind('keydown', keys, handler);
        },

        handleNewNote: function() {
            window.location.hash = '#';
        },

        broadCastEscapeHandler: function() {
            app.Events.Global.Hotkeys.Escape.trigger();
        },

        handleSearch: function() {
            new app.SearchPopupView();
        }
    });
});