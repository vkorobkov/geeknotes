var app = app || {};

$(function($) {
    app.LimitExceededWarningView = Backbone.View.extend({
            el: '#editor',
            isShowing: false,
            MAX_LENGTH: 65535,

            initialize: function() {
                // Setup correct max length when the model is ready
                app.Events.Global.ApplicationStarted.once(this.setupModel, this);

                // Reacting on value change event of editor
                app.Events.View.Editor.Change.on(this.onValueChanged, this);
            },

            setupModel: function() {
                var model = app.noteModel;
                this.MAX_LENGTH = model.BODY_MAX_LENGTH;
            },

            onValueChanged: function (editorEventData) {
                var value = editorEventData.getValue();

                if (value.length > this.MAX_LENGTH) {
                    this.showWarning();
                }
                else {
                    this.hideWarning();
                }
            },

            showWarning: function() {
                if (this.isShowing) {
                    return;
                }

                this.isShowing = true;
                var self = this;

                this.$el.notify({
                    type: 'editor-warning',
                    message: { html:
                        'Warning, dear user! You have exceeded the limit of 65,535 symbols for the note.<br>' +
                        'Please, reduce this document to 65,535 symbols length or less. Otherwise document <strong>will not be saved entirely.</strong>.'
                    },
                    fadeOut: { enabled: false },
                    onClosed: function() {
                        self.isShowing = false;
                    }
                }).show();
            },

            hideWarning: function() {
                if (this.isShowing) {
                    this.isShowing = false;
                    $('.alert-editor-warning', this.$el).remove();
                }
            }
        }
    );
});