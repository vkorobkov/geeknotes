var app = app || {};

$(function($) {
    app.Editor = Backbone.View.extend({
            el: '#editor',

            initialize: function() {
                // Create and initialize ace9 based MD editor
                this.editor = this.initializeAceEditor();

                // Init the model
                app.Events.Global.ApplicationStarted.once(this.setupModel, this);
            },

            setupModel: function() {
                var model = app.noteModel;
                // Handle start of any ajax - making the editor readonly
                model.onStartAjaxCallback = function() {
                    this.editor.setReadOnly(true);
                }.bind(this);
                // And on the end of ajax - enable it back again
                var enableEditorFunc = this.editor.setReadOnly.bind(this.editor, false);
                model.on('sync', enableEditorFunc);
                model.on('error', enableEditorFunc);

                // Callback for changing the hash
                model.windowHashChangeActor = function(hash) {
                    window.location.hash = hash;
                };

                // Limit exceeded callback
                model.onLimitExceededCallback = function() {
                    alert('You have exceeded the limit of maximum symbols. So only first 65535 were saved')
                };

                // On change model's body
                model.on('change:body', function(model, newBody) {
                    if ($.trim(newBody) === $.trim(this.editor.getValue())) {
                        return;
                    }
                    this.editor.setValue(newBody, 1);
                    this.editor.focus();
                }, this);

                // Listening to the hashchanges
                $(window).on('hashchange', this.onHashChange);
                // Set initial state
                this.onHashChange();
            },

            onHashChange: function() {
                var model = app.noteModel;
                var hash = window.location.hash;
                if (hash == null || hash.length < 2) {
                    model.set('id', null);
                }
                else {
                    model.set('id', hash.substring(1));
                }
            },

            initializeAceEditor: function() {
                // Define some globals
                var editor = ace.edit('editor');
                var session = editor.getSession();

                // Set font size
                document.getElementById('editor').style.fontSize='28px';

                // Configure session
                session.setMode('ace/mode/markdown');
                session.setUseWrapMode(true);

                // Configure editor
                editor.setTheme('ace/theme/tomorrow_night_eighties');
                editor.setShowPrintMargin(false);

                // Init IntelliJ idea-like hotkeys
                this.customizeHotKeys(editor);

                // Broadcast events from the editor
                session.on('change', function() {
                    app.Events.View.Editor.Change.trigger(new app.EditorEventData(editor));
                });
                session.selection.on('changeCursor', function() {
                    app.Events.View.Editor.ChangeCursor.trigger(new app.EditorEventData(editor));
                });

                return editor;
            },

            customizeHotKeys: function(editor) {
                var commands = editor.commands.commands;

                editor.commands.removeCommands(
                    commands.showSettingsMenu,
                    commands.goToNextError,
                    commands.goToPreviousError,
                    commands.centerselection,
                    commands.unfold,
                    commands.toggleFoldWidget,
                    commands.toggleParentFoldWidget,
                    commands.foldall,
                    commands.foldOther,
                    commands.unfoldall,
                    commands.findnext,
                    commands.findprevious,
                    commands.selectOrFindNext,
                    commands.selectOrFindPrevious,
                    commands.find,
                    commands.togglerecording,
                    commands.replaymacro,
                    commands.jumptomatching,
                    commands.selecttomatching,
                    commands.togglecomment,
                    commands.toggleBlockComment,
                    commands.replace,
                    commands.modifyNumberUp,
                    commands.modifyNumberDown,
                    commands.copylinesup,
                    commands.copylinesdown,
                    commands.transposeletters,
                    commands.fold
                );

                this.changeCommandHotKeys(editor, commands.gotoline, 'Ctrl-G', 'Command-G');
                this.changeCommandHotKeys(editor, commands.removeline, 'Ctrl-Y', 'Command-Y');
                this.changeCommandHotKeys(editor, commands.duplicateSelection, 'Ctrl-D', 'Command-D');
                this.changeCommandHotKeys(editor, commands.movelinesup, 'Ctrl-Shift-Up', 'Alt-Shift-Up');
                this.changeCommandHotKeys(editor, commands.movelinesdown, 'Ctrl-Shift-Down', 'Alt-Shift-Down');
                this.changeCommandHotKeys(editor, commands.splitline, 'Ctrl-Shift-Enter', 'Command-Shift-Enter');
                this.changeCommandHotKeys(editor, commands.findAll, 'Ctrl-Shift-F7', 'Command-Shift-F7');
            },

            changeCommandHotKeys: function(editor, command, win, mac) {
                command.bindKey.win = win;
                command.bindKey.mac = mac;
                editor.commands.addCommand(command);
            }
        }
    );
});