var app = app || {};

$(function($) {
    app.EditorFooterBar = Backbone.View.extend({
            el: '#editor-footer-bar',
            cursorEl: $('#editor-footer-bar-cursor'),
            symbolsLeftEl: $('#editor-footer-bar-left'),
            MAX_LENGTH: 65535,

            initialize: function() {
                // Setup correct max length when the model is ready
                app.Events.Global.ApplicationStarted.once(this.setupModel, this);

                // Subscribe to editor events
                app.Events.View.Editor.ChangeCursor.on(this.onCursorUpdate, this);
                app.Events.View.Editor.Change.on(this.onValueChanged, this);
            },

            setupModel: function() {
                var model = app.noteModel;
                this.MAX_LENGTH = model.BODY_MAX_LENGTH;
            },

            onValueChanged: function (editorEventData) {
                var value = editorEventData.getValue();
                var charsLeft = this.MAX_LENGTH - value.length;

                if (charsLeft < 0) {
                    this.symbolsLeftEl.text('No characters left. Please free ' + -1 * charsLeft + ' characters');
                }
                else {
                    this.symbolsLeftEl.text(charsLeft + ' characters left.');
                }
            },

            onCursorUpdate: function (editorEventData) {
                var cursor = editorEventData.getCursor();
                var row = cursor.row + 1;
                var column = cursor.column + 1;
                this.cursorEl.text('row ' + row + ', column ' + column);
            }
        }
    );
});