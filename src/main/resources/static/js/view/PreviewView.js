var app = app || {};

$(function($) {
    app.Preview = Backbone.View.extend({
            el: '#preview',
            currentWindowTitle: window.document.title,
            DEFAULT_BRAND: $("meta[name='product_brand']").attr('content'),

            initialize: function() {
                // Tune up MD renderer
                marked.setOptions({
                    breaks: true
                });

                // Declare postponed rendering function
                var postponedRender = app.Utils.createTimeOutTriggerFunction(300, this.previewTimerHandler, this);

                // Subscribe
                app.Events.View.Editor.Change.on(postponedRender);
                app.Events.View.Editor.ChangeCursor.on(postponedRender);
            },

            previewTimerHandler: function(editorEventData) {
                var scrollElementId = 'scrollToMe_' + app.Utils.uuid();
                var rowIndex = editorEventData.getCursor().row;
                var value = editorEventData.getValue();

                this.setPreviewHtml(this.generatePreviewHtmlWithScrollable(value, rowIndex, scrollElementId));
                this.updateScrolling('#' + scrollElementId);
                this.setPreviewHtml(marked(value));

                this.updateWindowTitle();
            },
            
            generatePreviewHtmlWithScrollable: function (value, rowIndex, scrollElementId) {
                var scrollElement = '<span id="' + scrollElementId + '"/>';
                var textAsLines = value.split('\n');
                textAsLines[rowIndex] = scrollElement + textAsLines[rowIndex];
                return marked(textAsLines.join('\n'));
            },

            setPreviewHtml: function(html) {
                this.$el.html(html);
                this.$el.find('table').addClass('table');
            },

            updateWindowTitle: function() {
                var windowTitle = 'GeekNotes';
                try {
                    windowTitle = this.$el.find('h1').get(0).innerText + ' - ' + windowTitle;
                }
                catch(e) {
                    windowTitle = 'Unnamed - ' + windowTitle;
                }
                this.setWindowTitle(windowTitle);
            },

            setWindowTitle: function(newTitle) {
                if (newTitle !== this.currentWindowTitle) {
                    this.currentWindowTitle = newTitle;
                    window.document.title = newTitle;
                }
            },

            updateScrolling: function(scrollElementId) {
                var scrollElement = $(scrollElementId);
                if (!this.isElementInTheVisibleArea(scrollElement)) {
                    this.$el.scrollTo(scrollElementId);
                }
                scrollElement.remove();
            },

            isElementInTheVisibleArea: function(elem) {
                var elemTop = elem.offset().top - this.$el.offset().top;
                return elemTop > 0 && (elemTop + 100) < this.$el.height();
            }
        }
    );
});