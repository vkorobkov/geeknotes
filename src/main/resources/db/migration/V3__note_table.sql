CREATE TABLE `note` (
	`id` BIGINT NOT NULL AUTO_INCREMENT,
	`user_id` INT NOT NULL,
	`is_note_id` VARCHAR(40) NOT NULL,
	`body` TEXT NULL,
	`header` VARCHAR(30) NULL,
	`is_empty` BIT NOT NULL,
	`cr_date` TIMESTAMP NOT NULL,
	`modified_date` TIMESTAMP NOT NULL,
	`version` BIGINT NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `esNoteId` (`is_note_id`),
	INDEX `is_empty` (`is_empty`),
	INDEX `modified_date` (`modified_date`),
	CONSTRAINT `note_to_user_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE NO ACTION
)
	COLLATE='utf8_general_ci'
	ENGINE=InnoDB
;