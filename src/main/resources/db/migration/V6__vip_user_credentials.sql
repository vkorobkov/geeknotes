CREATE TABLE `vip_user_credentials` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL,
	`password` VARCHAR(100) NOT NULL,
	`user_id` INT NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `name` (`name`),
	CONSTRAINT `vip_user_credentials_to_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE NO ACTION
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
