INSERT INTO `geek`.`user` (`id`, `role`) VALUES (2, 'ROLE_USER');

INSERT INTO `geek`.`vip_user_credentials` (`name`, `password`, `user_id`) VALUES ('guest', '$2a$10$Sbd1VcLpL9ruezVW0.cRtOZY2GZ7063u1bERTC4tpK14AG3QIidHO', 2);