CREATE TABLE `tag` (
	`id` BIGINT NOT NULL AUTO_INCREMENT,
	`value` VARCHAR(30) NOT NULL,
	`user_id` INT NOT NULL,
	`note_id` BIGINT NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `value` (`value`),
	CONSTRAINT `tag_to_user_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE NO ACTION,
	CONSTRAINT `tag_to_note_fk` FOREIGN KEY (`note_id`) REFERENCES `note` (`id`) ON UPDATE NO ACTION
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
