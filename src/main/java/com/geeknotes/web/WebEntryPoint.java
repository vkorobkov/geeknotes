package com.geeknotes.web;

import com.geeknotes.web.web.config.RootWebConfig;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

public class WebEntryPoint extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        application.sources(RootWebConfig.class);
        return application.listeners();
    }
}
