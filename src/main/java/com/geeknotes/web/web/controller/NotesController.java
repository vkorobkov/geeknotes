package com.geeknotes.web.web.controller;

import com.geeknotes.web.component.CurrentUserInfo;
import com.geeknotes.web.model.elastic.EsNote;
import com.geeknotes.web.model.sql.Note;
import com.geeknotes.web.service.NotesServiceFacade;
import com.geeknotes.web.service.exception.ESEntityNotFoundException;
import com.geeknotes.web.service.exception.SqlEntityNotFoundException;
import com.geeknotes.web.service.util.FacetedPageFacade;
import com.geeknotes.web.util.EasyStopWatch;
import com.geeknotes.web.util.LogUtils;
import com.geeknotes.web.web.SearchResultsConverter;
import com.geeknotes.web.web.dto.NoteSaveRequest;
import com.geeknotes.web.web.dto.NoteSearchResult;
import com.geeknotes.web.web.exception.BadRequestException;
import com.geeknotes.web.web.parsing.SearchQuery;
import com.geeknotes.web.web.parsing.SearchQueryParser;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.FacetedPage;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.ALL_VALUE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Controller
@RequestMapping(value = "/editor/notes", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
@Slf4j
public class NotesController {

    @Autowired
    private NotesServiceFacade notesServiceFacade;

    @Autowired
    private CurrentUserInfo currentUserInfo;

    @RequestMapping(value = "/note/", method = RequestMethod.POST)
    @ResponseBody
    public Object create(@RequestBody NoteSaveRequest saveRequest) {
        LogUtils.debugIfEnabled(log, () -> "Creating a new note with payload = " + saveRequest);

        EsNote note = notesServiceFacade.create(saveRequest.getBody(), currentUserInfo.getUser());
        return Collections.singletonMap("id", note.getId());
    }

    @RequestMapping(value = "/note/{id}", method = RequestMethod.PUT)
    @ResponseBody
    private Object update(@RequestBody NoteSaveRequest saveRequest, @PathVariable("id") String id) {
        LogUtils.debugIfEnabled(log, ()-> "Updating note: " + saveRequest);

        EsNote note = notesServiceFacade.update(saveRequest.getId(), saveRequest.getBody(), currentUserInfo.getUser());
        return Collections.singletonMap("id", note.getId());
    }

    @RequestMapping(value = "/note/{id}", method = RequestMethod.GET, consumes = ALL_VALUE)
    @ResponseBody
    public Object get(@PathVariable("id") String id) {
        log.info("Search note by esNoteId: " + id);
        EasyStopWatch stopWatch = new EasyStopWatch().start();

        Note note = notesServiceFacade.findNoteOfUser(id, currentUserInfo.getUserId());

        Map<String, Object> result = new LinkedHashMap<>(2);
        result.put("body", note.getBody());
        result.put("id", note.getEsNoteId());

        log.info("Search took " + stopWatch.stop());
        return result;
    }

    @RequestMapping(value = "/search/", method = RequestMethod.GET, consumes = ALL_VALUE)
    @ResponseBody
    public Object search(
            @RequestParam(value = "query", required = true) String payload,
            @RequestParam(value = "page", required = true) int page) {
        if (StringUtils.isBlank(payload)) {
            throw new BadRequestException("Search payload could not be empty");
        }

        log.info("Search for the note with request: " + payload);
        EasyStopWatch stopWatch = new EasyStopWatch().start();

        // Perform search
        SearchQuery searchQuery = new SearchQueryParser().parse(payload);
        LogUtils.debugIfEnabled(log, () -> "Parsed search query is: " + searchQuery);

        FacetedPage<EsNote> rawResult = notesServiceFacade.search(searchQuery, page, currentUserInfo.getUserId());
        LogUtils.debugIfEnabled(log, () -> "Search results: " + rawResult);

        // Highlight the results
        SearchResultsConverter resultsHighLighter = new SearchResultsConverter(searchQuery);
        List<NoteSearchResult> result = rawResult.getContent().stream().map(resultsHighLighter::convert)
                .collect(Collectors.toList());

        log.info("Search took " + stopWatch.stop());
        return new FacetedPageFacade(rawResult, result);
    }

    @RequestMapping(value = "/user-tags/", method = RequestMethod.GET, consumes = ALL_VALUE)
    @ResponseBody
    public Object getUniqueUserTags() {
        return notesServiceFacade.findDistinctTagsForUser(currentUserInfo.getUserId());
    }

    @ResponseStatus(value= HttpStatus.NOT_FOUND)
    @ExceptionHandler({SqlEntityNotFoundException.class, ESEntityNotFoundException.class})
    public void notFoundExceptionHandler() {
        // nothing actually to do here
    }
}