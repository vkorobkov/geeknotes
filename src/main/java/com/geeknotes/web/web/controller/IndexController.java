package com.geeknotes.web.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {

    @Value("${product.brand}")
    private String productBrand;

    @Autowired(required = false)
    @Value("${spring.thymeleaf.glueJs}")
    private boolean glueJs = true;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        return "redirect:/editor/";
    }

    @RequestMapping(value = "/editor/", method = RequestMethod.GET)
    public ModelAndView editor() {

        ModelAndView modelAndView = new ModelAndView("editor");
        modelAndView.addObject("product_brand", productBrand);
        modelAndView.addObject("glue_js", glueJs);
        return modelAndView;
    }
}
