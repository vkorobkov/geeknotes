package com.geeknotes.web.web.controller;

import com.geeknotes.web.service.NotesServiceFacade;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/admin")
@Slf4j
public class AdminController {

    @Autowired
    private NotesServiceFacade notesServiceFacade;

    @RequestMapping(value = "/reindex/", method = RequestMethod.GET)
    @ResponseBody
    public String reindex() {
        notesServiceFacade.reindex();
        return "OK";
    }
}
