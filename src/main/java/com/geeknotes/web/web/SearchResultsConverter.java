package com.geeknotes.web.web;


import com.geeknotes.web.model.elastic.EsNote;
import com.geeknotes.web.web.dto.NoteSearchResult;
import com.geeknotes.web.web.parsing.SearchQuery;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.joining;

public class SearchResultsConverter {

    private final static String TAG_NAME = "strong";
    private final static String TAG_START = "<" + TAG_NAME + ">";
    private final static String TAG_END =   "</" + TAG_NAME + ">";

    private final Set<String> tagsToHighLight;
    private final Set<String> tagsToHighLightLowerCase;
    private final String query;

    public SearchResultsConverter(SearchQuery query) {
        tagsToHighLight = new HashSet<>(query.getTags());
        this.tagsToHighLightLowerCase = tagsToHighLight.stream().map(tag -> tag.toLowerCase()).collect(Collectors.toSet());
        this.query = query.getQuery();
    }

    public NoteSearchResult convert(EsNote esNote) {
        NoteSearchResult result = new NoteSearchResult();

        // Some common fields
        result.setId(esNote.getId());

        // Highlight tags
        result.setTags(highLightTags(esNote.getTags()));

        // Highlight body & header
        if (!query.isEmpty()) {
            result.setHeader(highLightInStringIgnoreCase(esNote.getHeader(), query));
            result.setBody(highLightInStringIgnoreCase(esNote.getBody(), query));
        }
        else {
            result.setHeader(esNote.getHeader());
            result.setBody(esNote.getBody());
        }

        return result;
    }

    private List<String> highLightTags(Collection<String> tags) {
        return tags.stream().map(tag -> {
            if (tagsToHighLightLowerCase.contains(tag.toLowerCase())) {
                return highLightString(tag);
            } else {
                return tag;
            }
        }).collect(Collectors.toList());
    }

    private String highLightString(String string) {
        return TAG_START + string + TAG_END;
    }

    private String highLightInStringIgnoreCase(String string, String toHighLight) {
        Stream<String> linesStream = stream(string.split("\n"));
        Set<String> wordsToHighLight = stream(toHighLight.toLowerCase().split(" ")).collect(Collectors.toSet());

        linesStream = linesStream.map(line -> stream(line.split(" "))
                .map(word -> wordsToHighLight.contains(word.toLowerCase()) ? highLightString(word) : word)
                .collect(joining(" ")));

        return linesStream.collect(joining("\n"));
    }
}
