package com.geeknotes.web.web.util;


import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.resource.ResourceTransformer;
import org.springframework.web.servlet.resource.ResourceTransformerChain;
import org.springframework.web.servlet.resource.TransformedResource;
import ro.isdc.wro.model.resource.processor.ResourcePreProcessor;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.function.Supplier;

@AllArgsConstructor
@Slf4j
public class ResourceMinificationTransformer implements ResourceTransformer {

    // With no dots. Just like "js", "css"
    private final String resourceExtension;
    private final Supplier<ResourcePreProcessor> minifierSupplier;

    @Override
    public Resource transform(HttpServletRequest request, Resource resource, ResourceTransformerChain transformerChain) throws IOException {
        resource = transformerChain.transform(request, resource);

        if (shouldSkipTransformation(resource)) {
            return resource;
        }

        try {
            log.trace("Trying to minify resource: " + resource);
            return new TransformedResource(resource, minify(resource));
        }
        catch (Throwable exception) {
            log.error("Can not minify resource: " + resource, exception);
            log.error("Falling back to non-compressed version in order not to fail the whole application");
            return resource;
        }
    }

    protected byte[] minify(Resource resource) throws IOException {
        // Define some routine
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        InputStreamReader inputStreamReader = new InputStreamReader(resource.getInputStream());

        // Minify JS and return the result
        minifierSupplier.get().process(null, inputStreamReader, outputStreamWriter);
        return outputStream.toByteArray();
    }

    protected boolean shouldSkipTransformation(Resource resource) {
        String filename = resource.getFilename();

        // Skipping others extensions
        if (!resourceExtension.equals(StringUtils.getFilenameExtension(filename))) {
            return true;
        }

        // Skipping already minified
        if (filename.endsWith("min." + resourceExtension)) {
            return true;
        }

        return false;
    }
}
