package com.geeknotes.web.web.util;

import com.geeknotes.web.util.LogUtils;
import com.geeknotes.web.util.RegexpUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.Resource;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.resource.ResourceTransformerChain;
import org.springframework.web.servlet.resource.ResourceTransformerSupport;
import org.springframework.web.servlet.resource.TransformedResource;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Slf4j
public class TemplateLinksReplacementTransformer extends ResourceTransformerSupport {

    @Override
    public Resource transform(HttpServletRequest request, Resource resource, ResourceTransformerChain transformerChain) throws IOException {
        String filename = resource.getFilename();

        // Skipping others extensions
        if (!"js".equals(StringUtils.getFilenameExtension(filename))) {
            return resource;
        }

        LogUtils.traceIfEnabled(log, () -> "Going to read the JS resource in order to update the links to HTML templates. Resource: " + resource);

        String content = IOUtils.toString(resource.getInputStream());

        List<String> urlsToReplace = new RegexpUtils().matcherFindStream("'(/static/templates/(.*).html?)'", content)
                .map(matcher -> matcher.group(1)).collect(Collectors.toList());

        if (urlsToReplace.isEmpty()) {
            LogUtils.traceIfEnabled(log, () -> "Nothing to replace in resource :" + resource);
            return resource;
        }

        for (String urlToReplace: urlsToReplace) {
            String newPath = resolveUrlPath(urlToReplace, request, resource, transformerChain);
            content = content.replace(urlToReplace, newPath);
        }
        LogUtils.traceIfEnabled(log, () -> "Link(s) to html template(s) were replaced in the resource: " + resource);

        return new TransformedResource(resource, content.getBytes());
    }
}
