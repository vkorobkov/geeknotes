package com.geeknotes.web.web.parsing;


import lombok.Getter;
import lombok.ToString;

import java.util.List;

@ToString
@Getter
public class NotePayload {

    private final String tagLessHtml;
    private final String tagLessBody;
    private final String header;
    private final List<String> tags;

    public NotePayload(String tagLessHtml, String tagLessBody, String header, List<String> tags) {
        this.tagLessHtml = tagLessHtml;
        this.tagLessBody = tagLessBody;
        this.header = header;
        this.tags = tags;
    }

    public boolean isEmpty() {
        return tagLessBody.isEmpty() && header.isEmpty() && tags.isEmpty();
    }

    public boolean hasTags() {
        return !tags.isEmpty();
    }
}
