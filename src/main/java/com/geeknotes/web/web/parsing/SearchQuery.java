package com.geeknotes.web.web.parsing;

import lombok.Getter;
import lombok.ToString;

import java.util.Collections;
import java.util.List;

@ToString
@Getter
public class SearchQuery {

    private final List<String> tags;
    private final String query;

    public SearchQuery(List<String> tags, String query) {
        this.tags = Collections.unmodifiableList(tags);
        this.query = query;
    }

    public boolean hasTags() {
        return !tags.isEmpty();
    }

    public boolean hasQuery() {
        return !this.query.isEmpty();
    }
}
