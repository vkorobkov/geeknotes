package com.geeknotes.web.web.parsing;


import com.geeknotes.web.CommonConstants;
import com.github.rjeschke.txtmark.Processor;

import java.util.List;
import java.util.regex.Pattern;

public class NotePayloadParser {

    private static final Pattern REMOVE_HTML_TAGS = Pattern.compile("<.+?>");
    private static final String H1_START_TAG = "<h1>";
    private static final String H1_END_TAG = "</h1>";

    public NotePayload parse(String payload) {
        String tagLessHtml = getTagLessHtml(payload);
        String tagLessBody = getTagLessBody(tagLessHtml);
        String header = getHeader(tagLessHtml, tagLessBody);
        List<String> tags = ParseUtils.extractTags(payload);

        return new NotePayload(tagLessHtml, tagLessBody, header, tags);
    }

    private String getTagLessHtml(String payload) {
        String tagLessPayload = ParseUtils.cutTagsFromPayload(payload);
        return Processor.process(tagLessPayload);
    }

    private String getTagLessBody(String html) {
        html = REMOVE_HTML_TAGS.matcher(html).replaceAll("");
        html = ParseUtils.replaceNewLineSymbolsWithSpaces(html);
        html = ParseUtils.removeExtraSpaces(html);
        return html;
    }

    private String getHeader(String tagLessHtml, String tagLessBody) {
        int h1StartIndex = tagLessHtml.indexOf(H1_START_TAG);
        if (h1StartIndex >= 0) {
            int h1EndIndex = tagLessHtml.indexOf(H1_END_TAG, h1StartIndex);
            String h1 = tagLessHtml.substring(h1StartIndex + H1_START_TAG.length(), h1EndIndex);
            return ParseUtils.cropString(h1, CommonConstants.NOTE_HEADER_MAX_LEN);
        }
        return ParseUtils.cropString(tagLessBody, CommonConstants.NOTE_HEADER_MAX_LEN);
    }
}
