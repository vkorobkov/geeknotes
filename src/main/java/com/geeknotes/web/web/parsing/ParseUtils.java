package com.geeknotes.web.web.parsing;

import com.geeknotes.web.CommonConstants;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.joining;

public final class ParseUtils {

    private static final Pattern REMOVE_EXTRA_SPACES = Pattern.compile(" +");

    public static String removeExtraSpaces(String text) {
        text = REMOVE_EXTRA_SPACES.matcher(text).replaceAll(" ");
        text = text.trim();
        return text;
    }

    public static String replaceNewLineSymbolsWithSpaces(String text) {
        return text.replaceAll("\n", " ");
    }

    public static List<String> extractTags(String payload) {
        Set<String> tags = new LinkedHashSet<>();
        stream(payload.split("\n")).forEach(line -> {
            Set<String> tagsInLine = stream(line.split(" ")).filter(theWordIsTag()).collect(Collectors.toCollection(LinkedHashSet::new));
            tags.addAll(tagsInLine);
        });
        List<String> tagsList =
                tags.stream().map(word -> cropString(word.substring(1), CommonConstants.TAG_MAX_LEN)).collect(Collectors.toList());
        return Collections.unmodifiableList(tagsList);
    }

    public static String cropString(String s, int maxLength) {
        return s.substring(0, Math.min(s.length(), maxLength));
    }

    public static String cutTagsFromPayload(String payload) {
        return stream(payload.split("\n")).map(cuttingTagsMapper()).collect((joining("\n")));
    }

    private static Predicate<String> theWordIsTag() {
        return word -> word.startsWith("/");
    }

    private static Function<String, String> cuttingTagsMapper() {
        return line -> stream(line.split(" ")).filter(theWordIsNotTag()).collect(joining(" "));
    }

    private static Predicate<String> theWordIsNotTag() {
        return word -> !word.startsWith("/");
    }
}
