package com.geeknotes.web.web.parsing;

import java.util.List;

public class SearchQueryParser {

    public SearchQuery parse(String payload) {
        List<String> tags = ParseUtils.extractTags(payload);
        String query = ParseUtils.cutTagsFromPayload(payload);
        query = ParseUtils.removeExtraSpaces(query);
        return new SearchQuery(tags, query);
    }
}
