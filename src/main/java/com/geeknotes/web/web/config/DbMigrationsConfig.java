package com.geeknotes.web.web.config;

import com.geeknotes.web.util.EasyStopWatch;
import com.googlecode.flyway.core.Flyway;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

@Configuration
@Slf4j
public class DbMigrationsConfig {

    @Autowired
    private DataSource dataSource;

    @PostConstruct
    private void doMigrations() {
        EasyStopWatch stopWatch = new EasyStopWatch().start();

        Flyway flyway = new Flyway();
        flyway.setDataSource(dataSource);
        flyway.migrate();

        log.info("Database migration successfully done. It took " + stopWatch.stop());
    }
}
