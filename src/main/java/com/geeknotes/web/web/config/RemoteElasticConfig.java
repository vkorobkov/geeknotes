package com.geeknotes.web.web.config;

import com.geeknotes.web.repo.elastic.ElasticRepoPackageInfo;
import com.geeknotes.web.util.EasyStopWatch;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.transport.TransportAddress;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@Configuration
@EnableElasticsearchRepositories(basePackageClasses = ElasticRepoPackageInfo.class)
@ConditionalOnProperty(name = "es.local", havingValue = "false", matchIfMissing = true)
@Slf4j
public class RemoteElasticConfig implements ElasticConfigBase {

    @Value("${es.remote.host}")
    private String esRemoteHost;

    @Value("${es.remote.port}")
    private int esRemotePort;

    @Override
    @Bean
    public Client elasticSearchClient() {
        log.info("Connecting remote elastic search client: {}:{}", esRemoteHost, esRemotePort);
        EasyStopWatch stopWatch = new EasyStopWatch().start();

        TransportAddress address = new InetSocketTransportAddress(esRemoteHost, esRemotePort);
        TransportClient client = new TransportClient().addTransportAddress(address);
        if (client.connectedNodes().size() == 0) {
            throw new IllegalStateException("There is no ElasticSearch nodes connected. Failed to start");
        }

        log.info("Creating remote elastic search client is done. It took {}", stopWatch.stop());
        return client;
    }
}
