package com.geeknotes.web.web.config;

import com.geeknotes.web.repo.elastic.ElasticRepoPackageInfo;
import com.geeknotes.web.util.EasyStopWatch;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.client.Client;
import org.elasticsearch.node.Node;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

import static org.elasticsearch.node.NodeBuilder.nodeBuilder;

@Configuration
@EnableElasticsearchRepositories(basePackageClasses = ElasticRepoPackageInfo.class)
@ConditionalOnProperty(name = "es.local", havingValue = "true")
@Slf4j
public class LocalElasticConfig implements ElasticConfigBase {

    @Bean
    public Node elasticSearchNode() {
        EasyStopWatch stopWatch = new EasyStopWatch().start();
        log.info("Starting ElasticSearch local node");

        Node node = nodeBuilder().node();

        log.info("ElasticSearch local node start took " + stopWatch.stop());
        return node;
    }

    @Override
    @Bean
    public Client elasticSearchClient() {
        return elasticSearchNode().client();
    }

    @Bean
    public ElasticsearchTemplate elasticsearchTemplate() {
        return new ElasticsearchTemplate(elasticSearchClient());
    }
}
