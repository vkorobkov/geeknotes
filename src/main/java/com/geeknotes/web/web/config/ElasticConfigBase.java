package com.geeknotes.web.web.config;

import org.elasticsearch.client.Client;

import javax.annotation.PreDestroy;


public interface ElasticConfigBase {

    Client elasticSearchClient();

    @PreDestroy
    default void cleanUpElasticSearchClient() {
        elasticSearchClient().close();
    }
}
