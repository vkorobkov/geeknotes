package com.geeknotes.web.web.config;

import com.geeknotes.web.component.ProductionJSAssembler;
import com.geeknotes.web.web.util.ResourceMinificationTransformer;
import com.geeknotes.web.web.util.TemplateLinksReplacementTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.resource.*;
import ro.isdc.wro.model.resource.processor.impl.css.CssMinProcessor;
import ro.isdc.wro.model.resource.processor.impl.js.JSMinProcessor;

import java.util.Optional;

@Configuration
@EnableWebMvc
public class MvcConfig extends WebMvcConfigurerAdapter {

    @Autowired
    @Value("${spring.thymeleaf.resourcesPath}")
    private String staticResourceLocation = "classpath:/static/";

    @Autowired(required = false)
    @Value("${spring.thymeleaf.useCompressedCss}")
    private boolean useCompressedCss = true;

    @Autowired(required = false)
    @Value("${spring.thymeleaf.useCompressedJs}")
    private boolean useCompressedJs = true;

    @Autowired(required = false)
    @Value("${spring.thymeleaf.useVersionStrategy}")
    private boolean userVersionStrategy = true;

    @Autowired(required = false)
    @Value("${spring.thymeleaf.glueJs}")
    private boolean glueJs = true;

    @Autowired
    private Optional<ProductionJSAssembler> productionJsAssembler;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        ResourceHandlerRegistration handlerRegistration = registry.addResourceHandler("/static/**")
                .addResourceLocations(staticResourceLocation);

        if (glueJs) {
            handlerRegistration.addResourceLocations(productionJsAssembler.get().getProductionFolderURI().toString());
        }

        ResourceChainRegistration registration = handlerRegistration.setCachePeriod(Integer.MAX_VALUE)
                .resourceChain(true)
                .addResolver(new GzipResourceResolver());

        if (userVersionStrategy) {
            registration.addResolver(new VersionResourceResolver()
                    .addVersionStrategy(new NullVersionStrategy(), "/js/libs/**", "/css/bootstrap*.css", "/fonts/**", "/images/**")
                    .addContentVersionStrategy("/js/model/**", "/js/util/**", "/js/view/**", "/js/app*.js", "/css/common*.css", "/templates/**"));
            registration.addTransformer(new TemplateLinksReplacementTransformer());
        }

        registration.addResolver(new PathResourceResolver());

        if (useCompressedCss) {
            registration.addTransformer(new ResourceMinificationTransformer("css", CssMinProcessor::new));
        }
        if (useCompressedJs) {
            registration.addTransformer(new ResourceMinificationTransformer("js", JSMinProcessor::new));
        }
    }

    @Bean
    public ResourceUrlEncodingFilter resourceUrlEncodingFilter() {
        return new ResourceUrlEncodingFilter();
    }

    private static class NullVersionStrategy extends AbstractVersionStrategy {

        protected NullVersionStrategy() {
            super(new VersionPathStrategy() {

                @Override
                public String extractVersion(String requestPath) {
                    return "";
                }

                @Override
                public String removeVersion(String requestPath, String version) {
                    return requestPath;
                }

                @Override
                public String addVersion(String requestPath, String version) {
                    return requestPath;
                }
            });
        }

        @Override
        public String getResourceVersion(Resource resource) {
            return "";
        }
    }
}
