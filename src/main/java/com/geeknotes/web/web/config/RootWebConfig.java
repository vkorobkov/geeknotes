package com.geeknotes.web.web.config;

import com.geeknotes.web.JavaEntryPoint;
import com.geeknotes.web.model.sql.SqlModelPackageInfo;
import com.geeknotes.web.repo.sql.SqlRepoPackageInfo;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.elasticsearch.ElasticsearchAutoConfiguration;
import org.springframework.boot.autoconfigure.web.EmbeddedServletContainerAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableAutoConfiguration(exclude = {
        EmbeddedServletContainerAutoConfiguration.class,
        EmbeddedServletContainerAutoConfiguration.EmbeddedTomcat.class,
        ElasticsearchAutoConfiguration.class
})
@EntityScan(basePackageClasses = SqlModelPackageInfo.class)
@EnableJpaRepositories(basePackageClasses = SqlRepoPackageInfo.class)
@ComponentScan(
        basePackageClasses = JavaEntryPoint.class,
        excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = RootJavaConfig.class)
)
@Import(CommonConfig.class)
public class RootWebConfig {
}
