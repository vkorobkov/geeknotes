package com.geeknotes.web.web.dto;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class NoteSaveRequest {

    private String id;
    private String body;
}
