package com.geeknotes.web.web.dto;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class NoteSearchResult {
    private String id;
    private List<String> tags;
    private String header;
    private String body;
}
