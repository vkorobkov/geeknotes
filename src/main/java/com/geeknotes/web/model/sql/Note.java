package com.geeknotes.web.model.sql;

import com.geeknotes.web.CommonConstants;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@ToString(exclude = "tags")
public class Note {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @NotEmpty
    private String esNoteId;

    @Length(max = CommonConstants.NOTE_BODY_MAX_LEN)
    private String body;

    @Length(max = CommonConstants.NOTE_HEADER_MAX_LEN)
    private String header;

    private boolean isEmpty;

    @CreatedDate
    private Date crDate;

    @LastModifiedDate
    private Date modifiedDate;

    @Version
    private Long version;

    @ManyToOne(fetch = FetchType.EAGER)
    private User user;

    @Column(name="user_id", insertable = false, updatable = false)
    private Long userId;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "note")
    private List<Tag> tags = new ArrayList<>();
}
