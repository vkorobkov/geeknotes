package com.geeknotes.web.model.sql;

import com.geeknotes.web.CommonConstants;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@ToString
public class Tag {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @NotEmpty
    @Length(max = CommonConstants.TAG_MAX_LEN)
    private String value;

    @ManyToOne(fetch = FetchType.EAGER)
    private User user;

    @ManyToOne(fetch = FetchType.EAGER)
    private Note note;

    @Column(name="user_id", insertable = false, updatable = false)
    private Long userId;
}
