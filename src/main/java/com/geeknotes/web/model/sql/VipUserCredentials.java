package com.geeknotes.web.model.sql;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@ToString(exclude = "user")
public class VipUserCredentials {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @NotEmpty
    @Length(max = 50)
    private String name;

    @NotNull
    @NotEmpty
    @Length(max = 100)
    private String password;

    @OneToOne(fetch = FetchType.EAGER)
    private User user;
}
