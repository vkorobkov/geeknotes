package com.geeknotes.web.model.elastic;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.ArrayList;
import java.util.Collection;

@Document(indexName = "notes", type = "note")
@Getter
@Setter
public class EsNote {

    @Id
    private String id;

    @Field(type = FieldType.Long, store = true)
    private Long userId;

    @Field(type = FieldType.String, store = false)
    private String body;

    @Field(type = FieldType.String, store = true)
    private String header;

    @Field(type = FieldType.String, store = true)
    private Collection<String> tags = new ArrayList<>();

    @Field(type = FieldType.Boolean, store = true)
    private Boolean isEmpty;
}
