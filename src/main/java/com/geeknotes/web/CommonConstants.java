package com.geeknotes.web;

public class CommonConstants {

    public static final int NOTE_HEADER_MAX_LEN = 30;
    public static final int TAG_MAX_LEN = 30;
    public static final int NOTE_BODY_MAX_LEN = 65_535;
}
