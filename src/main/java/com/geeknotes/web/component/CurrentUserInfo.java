package com.geeknotes.web.component;

import com.geeknotes.web.model.sql.User;
import com.geeknotes.web.repo.sql.UserRepository;
import com.geeknotes.web.util.SecuredUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class CurrentUserInfo {

    @Autowired
    private UserRepository userRepository;

    public Long getUserId() {
        return getUser().getId();
    }

    public User getUser() {
        SecuredUser user = (SecuredUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userRepository.findOne(user.getUserId());
    }
}
