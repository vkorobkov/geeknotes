package com.geeknotes.web.component;

import com.geeknotes.web.util.ClassPathUtils;
import com.geeknotes.web.util.EasyStopWatch;
import com.geeknotes.web.util.RegexpUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

@Component
@ConditionalOnProperty(name = "spring.thymeleaf.glueJs", havingValue = "true", matchIfMissing = true)
@Slf4j
public class ProductionJSAssembler {

    private Path tempDirectory;

    @PostConstruct
    private void setup() throws IOException, URISyntaxException {
        EasyStopWatch stopWatch = new EasyStopWatch().start();

        final String TEMP_DIRECTORY_PREXIX = "geeknotes-production-static-";
        final String GLUED_JS_FILE_NAME = "app-all.js";

        // Creating directories structure and empty production file
        log.info("Creating directories structure for production resources");
        tempDirectory = Files.createTempDirectory(TEMP_DIRECTORY_PREXIX);
        Path jsDirectory = Files.createDirectories(tempDirectory.resolve("js"));
        Path gluedJs = Files.createFile(jsDirectory.resolve(GLUED_JS_FILE_NAME));

        // Gluing multiple javascript's into one production file
        log.info("About to start concatenating JavaScript");
        ClassPathUtils classPathUtils = new ClassPathUtils();
        File dstFile = gluedJs.toFile();
        getFilesToCompress().forEach(file -> {
            try {
                File srcFile = classPathUtils.getNioPath(file).toFile();
                FileUtils.write(dstFile, FileUtils.readFileToString(srcFile), true);
            } catch (IOException | URISyntaxException exception) {
                throw new RuntimeException("Unable to concatenate resource: " + file, exception);
            }
        });

        log.info("Creating production JavaScript file took " + stopWatch.stop());
    }

    public URI getProductionFolderURI() {
        return tempDirectory.toUri();
    }

    private Stream<String> getFilesToCompress() throws IOException, URISyntaxException {
        final String LIBS_REGEXP = "\\{/(?!static/js/libs/|static/js/app-all.js)(.*?)\\}";
        return new RegexpUtils().matcherFindStream(LIBS_REGEXP, readHtmlTemplateWithIncludes())
                .map(matcher -> matcher.group(1));
    }

    private String readHtmlTemplateWithIncludes() throws IOException, URISyntaxException {
        final String HTML_PATH = "templates/js-includes.html";
        return FileUtils.readFileToString(new ClassPathUtils().getNioPath(HTML_PATH).toFile());
    }

    @PreDestroy
    private void cleanupTempFolder() throws IOException {
        log.info("About to remove temporary directory with static resources");
        FileUtils.deleteDirectory(tempDirectory.toFile());
    }
}
