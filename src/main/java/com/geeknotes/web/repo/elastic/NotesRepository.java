package com.geeknotes.web.repo.elastic;


import com.geeknotes.web.model.elastic.EsNote;
import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;

public interface NotesRepository extends ElasticsearchCrudRepository<EsNote, String> {

    EsNote findOneByIdAndUserId(String id, Long userId);
}
