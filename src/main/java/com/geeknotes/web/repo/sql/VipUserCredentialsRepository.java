package com.geeknotes.web.repo.sql;

import com.geeknotes.web.model.sql.VipUserCredentials;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VipUserCredentialsRepository extends JpaRepository<VipUserCredentials, Long> {

    VipUserCredentials findByName(String name);
}
