package com.geeknotes.web.repo.sql;

import com.geeknotes.web.model.sql.Note;
import com.geeknotes.web.util.LongJpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NoteRepository extends LongJpaRepository<Note> {

    Note findOneByEsNoteId(String esNoteId);

    Note findOneByEsNoteIdAndUserId(String esNoteId, Long userId);
}
