package com.geeknotes.web.repo.sql;

import com.geeknotes.web.model.sql.Tag;
import com.geeknotes.web.util.LongJpaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TagRepository extends LongJpaRepository<Tag> {

    // Example of how to use it
    // tagRepository.findDistinctTagsForUser(1L, new PageRequest(0, 100, new Sort(Sort.Direction.ASC, "value")))
    @Query("SELECT DISTINCT t.value FROM Tag t WHERE t.userId = :userId")
    Page<String> findDistinctTagsForUser(@Param("userId") Long userId, Pageable pageable);

    @Query("SELECT DISTINCT t.value FROM Tag t WHERE t.userId = :userId")
    String[] findDistinctTagsForUserOrderByValueAsc(@Param("userId") Long userId);
}
