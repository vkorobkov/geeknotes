package com.geeknotes.web.util;


import org.slf4j.Logger;

import java.util.function.Supplier;

public class LogUtils {

    public static void debugIfEnabled(Logger logger, Supplier<String> messageSupplier) {
        if (logger.isDebugEnabled()) {
            logger.debug(messageSupplier.get());
        }
    }

    public static void traceIfEnabled(Logger logger, Supplier<String> messageSupplier) {
        if (logger.isTraceEnabled()) {
            logger.trace(messageSupplier.get());
        }
    }
}
