package com.geeknotes.web.util;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class RegexpUtils {

    public Stream<Matcher> matcherFindStream(String pattern, String input) {
        return matcherFindStream(Pattern.compile(pattern), input);
    }

    public Stream<Matcher> matcherFindStream(Pattern pattern, String input) {
        Matcher matcher = pattern.matcher(input);
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(new Iterator<Matcher>() {
            @Override
            public boolean hasNext() {
                return matcher.find();
            }

            @Override
            public Matcher next() {
                return matcher;
            }
        }, Spliterator.ORDERED), false);
    }
}
