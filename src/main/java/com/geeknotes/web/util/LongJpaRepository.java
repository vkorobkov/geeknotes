package com.geeknotes.web.util;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LongJpaRepository<T> extends JpaRepository<T, Long> {
}
