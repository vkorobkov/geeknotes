package com.geeknotes.web.util;


import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class SecuredUser extends User {

    private final Long userId;

    public SecuredUser(Long userId, String username, String password, String authorities) {
        this(userId, username, password, AuthorityUtils.commaSeparatedStringToAuthorityList(authorities));
    }

    public SecuredUser(Long userId, String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }
}
