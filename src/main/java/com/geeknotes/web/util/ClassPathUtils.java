package com.geeknotes.web.util;

import org.springframework.util.ResourceUtils;

import java.io.FileNotFoundException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ClassPathUtils {

    public Path getNioPath(String resource) throws FileNotFoundException, URISyntaxException {
        return Paths.get(getURI(resource));
    }

    public URI getURI(String resource) throws FileNotFoundException, URISyntaxException {
        return getURL(resource).toURI();
    }

    public URL getURL(String resource) throws FileNotFoundException {
        return ResourceUtils.getURL(ResourceUtils.CLASSPATH_URL_PREFIX + resource);
    }
}
