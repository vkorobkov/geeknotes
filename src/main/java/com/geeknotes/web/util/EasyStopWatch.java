package com.geeknotes.web.util;

// Not THREAD SAFE stopwatch
public class EasyStopWatch {

    private long startTime;
    private long totalTime;

    public EasyStopWatch start() {
        startTime = System.currentTimeMillis();
        return this;
    }

    public EasyStopWatch stop() {
        totalTime = System.currentTimeMillis() - startTime;
        return this;
    }

    public long totalMilliSeconds() {
        return totalTime;
    }

    @Override
    public String toString() {
        return totalTime + " millisecond(s)";
    }
}
