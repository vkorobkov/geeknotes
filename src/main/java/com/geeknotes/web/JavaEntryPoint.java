package com.geeknotes.web;

import com.geeknotes.web.web.config.RootJavaConfig;
import org.springframework.boot.SpringApplication;

public class JavaEntryPoint {
    public static void main(String[] args) {
        SpringApplication.run(RootJavaConfig.class, args);
    }
}
