package com.geeknotes.web.service;

import com.geeknotes.web.model.elastic.EsNote;
import com.geeknotes.web.repo.elastic.NotesRepository;
import com.geeknotes.web.service.exception.ESEntityNotFoundException;
import com.geeknotes.web.util.EasyStopWatch;
import com.geeknotes.web.util.LogUtils;
import com.geeknotes.web.web.parsing.NotePayload;
import com.geeknotes.web.web.parsing.SearchQuery;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.FacetedPage;
import org.springframework.data.elasticsearch.core.query.StringQuery;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchQuery;

@Service
@Slf4j
public class NotesElasticService {

    private static final int PAGE_SIZE = 10;

    @Autowired
    private NotesRepository notesRepository;

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    public EsNote create(NotePayload payload, Long userId) {
        return create(payload, userId, UUID.randomUUID().toString());
    }

    public EsNote create(NotePayload payload, Long userId, String esNoteId) {
        log.info("Instantiating new note and saving it into ES");
        EasyStopWatch stopWatch = new EasyStopWatch().start();

        EsNote esNote = new EsNote();
        esNote.setId(esNoteId);
        setEsNoteContent(payload, esNote, userId);

        esNote = notesRepository.save(esNote);

        log.info("New note has been created. It took " + stopWatch.stop());
        return esNote;
    }

    public EsNote update(String esNodeId, NotePayload payload, Long userId) {
        log.info("Instantiating new note and saving it into ES");
        EasyStopWatch stopWatch = new EasyStopWatch().start();

        EsNote esNote = notesRepository.findOneByIdAndUserId(esNodeId, userId);
        if (esNote == null) {
            throw new ESEntityNotFoundException("id", esNodeId, EsNote.class);
        }
        setEsNoteContent(payload, esNote, userId);
        esNote = notesRepository.save(esNote);

        log.info("Note has been updated. It took " + stopWatch.stop());
        return esNote;
    }

    public FacetedPage<EsNote> search(SearchQuery searchQuery, int page, Long userId) {
        log.info("Performing the notes search");
        LogUtils.debugIfEnabled(log, () -> "The query is: " + searchQuery + ", the page number is: " + page);
        EasyStopWatch stopWatch = new EasyStopWatch().start();

        BoolQueryBuilder boolQuery = boolQuery();

        boolQuery.must(matchQuery("userId", userId));
        boolQuery.mustNot(matchQuery("isEmpty", true));
        if (searchQuery.hasTags()) {
            boolQuery.must(matchQuery("tags", searchQuery.getTags().stream().collect(Collectors.joining(" ")) ).operator(MatchQueryBuilder.Operator.AND));
        }
        if (searchQuery.hasQuery()) {
            boolQuery.should(matchQuery("header", searchQuery.getQuery()));
            boolQuery.must(matchQuery("body", searchQuery.getQuery()));
        }

        LogUtils.debugIfEnabled(log, () -> "ES native query: " + boolQuery.toString());

        StringQuery esQuery = new StringQuery(boolQuery.toString(), new PageRequest(page, PAGE_SIZE));
        FacetedPage<EsNote> result = elasticsearchTemplate.queryForPage(esQuery, EsNote.class);

        log.info("Search took " + stopWatch.stop());
        LogUtils.debugIfEnabled(log, () -> "Search results: " + result);
        return result;
    }

    public void deleteById(String id) {
        notesRepository.delete(id);
    }

    public void deleteAll() {
        notesRepository.deleteAll();
    }

    private void setEsNoteContent(NotePayload payload, EsNote esNote, Long userId) {
        esNote.setBody(payload.getTagLessBody());
        esNote.setHeader(payload.getHeader());
        esNote.setTags(payload.getTags());
        esNote.setUserId(userId);
        esNote.setIsEmpty(payload.isEmpty());
    }
}
