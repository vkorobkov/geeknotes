package com.geeknotes.web.service;

import com.geeknotes.web.model.sql.User;
import com.geeknotes.web.model.sql.VipUserCredentials;
import com.geeknotes.web.repo.sql.VipUserCredentialsRepository;
import com.geeknotes.web.util.SecuredUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsLookupService implements UserDetailsService {

    @Autowired
    private VipUserCredentialsRepository vipUserCredentialsRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        VipUserCredentials credentials = vipUserCredentialsRepository.findByName(username);
        if (credentials == null) {
            throw new UsernameNotFoundException("Following user was not found: " + username);
        }
        User user = credentials.getUser();
        return new SecuredUser(user.getId(), username, credentials.getPassword(), user.getRole());
    }
}
