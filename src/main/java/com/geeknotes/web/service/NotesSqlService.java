package com.geeknotes.web.service;

import com.geeknotes.web.model.sql.Note;
import com.geeknotes.web.model.sql.Tag;
import com.geeknotes.web.model.sql.User;
import com.geeknotes.web.repo.sql.NoteRepository;
import com.geeknotes.web.repo.sql.TagRepository;
import com.geeknotes.web.service.exception.SqlEntityNotFoundException;
import com.geeknotes.web.util.EasyStopWatch;
import com.geeknotes.web.util.LogUtils;
import com.geeknotes.web.web.parsing.NotePayload;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class NotesSqlService {

    @Autowired
    private NoteRepository noteRepository;

    @Autowired
    private TagRepository tagRepository;

    @Transactional
    public Note create(NotePayload notePayload, String esNoteId, String rawBody, User currentUser) {
        log.info("About to create and save note to MySql");
        LogUtils.debugIfEnabled(log, () -> "Note payload: " + notePayload);
        LogUtils.debugIfEnabled(log, () -> "ElasticSearch note id: " + esNoteId);
        EasyStopWatch stopWatch = new EasyStopWatch().start();

        Note note = new Note();
        note.setEsNoteId(esNoteId);
        setNoteContentFields(note, notePayload, rawBody);
        note.setUser(currentUser);

        // Saving note
        note = noteRepository.save(note);

        // Saving tags
        saveTags(notePayload, note, currentUser);

        log.info("Saving note to MySql took " + stopWatch.stop());
        return note;
    }

    @Transactional
    public Note update(NotePayload notePayload, String esNoteId, String rawBody, User currentUser) {
        log.info("About to update note and tags in MySql");
        LogUtils.debugIfEnabled(log, () -> "Note payload: " + notePayload);
        LogUtils.debugIfEnabled(log, () -> "ElasticSearch note id: " + esNoteId);
        EasyStopWatch stopWatch = new EasyStopWatch().start();

        // Find note, clear tags and set content properties
        Note note = findOneByEsNoteIdAndUserId(esNoteId, currentUser.getId());
        setNoteContentFields(note, notePayload, rawBody);
        note.getTags().clear();
        note = noteRepository.save(note);

        // Delete all tags and create new ones
        saveTags(notePayload, note, currentUser);

        log.info("Saving note to MySql took " + stopWatch.stop());
        return note;
    }

    @Transactional(readOnly = true)
    public Note findOneByEsNoteIdAndUserId(String esNoteId, Long userId) {
        Note note = noteRepository.findOneByEsNoteIdAndUserId(esNoteId, userId);
        if (note == null) {
            throw new SqlEntityNotFoundException("esNoteId and userId", esNoteId + " and " + userId, Note.class);
        }
        return note;
    }

    @Transactional
    public void deleteNoteAndTagsByEsId(String esNoteId) {
        Note note = findByEsNoteId(esNoteId);
        note.getTags().clear();
        note = noteRepository.save(note);
        noteRepository.delete(note);
    }

    @Transactional(readOnly = true)
    public Page<Note> findAllAsPage(int page, int size) {
        return noteRepository.findAll(new PageRequest(page, size));
    }

    @Transactional(readOnly = true)
    public String[] findDistinctTagsForUser(Long userId) {
        return tagRepository.findDistinctTagsForUserOrderByValueAsc(userId);
    }

    private void setNoteContentFields(Note note, NotePayload notePayload, String rawBody) {
        note.setBody(rawBody);
        note.setHeader(notePayload.getHeader());
        note.setEmpty(notePayload.isEmpty());
    }

    private void saveTags(NotePayload notePayload, Note note, User currentUser) {
        // No tags - no problems :)
        if (!notePayload.hasTags()) {
            return;
        }

        List<Tag> tags = notePayload.getTags().stream().map(tagValue -> {
            Tag tag = new Tag();
            tag.setNote(note);
            tag.setUser(currentUser);
            tag.setValue(tagValue);
            return tag;
        }).collect(Collectors.toList());
        tagRepository.save(tags);
    }

    private Note findByEsNoteId(String esNoteId) {
        Note note = noteRepository.findOneByEsNoteId(esNoteId);
        if (note == null) {
            throw new SqlEntityNotFoundException("esNoteId", esNoteId, Note.class);
        }
        return note;
    }
}
