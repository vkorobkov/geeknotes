package com.geeknotes.web.service;

import com.geeknotes.web.model.elastic.EsNote;
import com.geeknotes.web.model.sql.Note;
import com.geeknotes.web.model.sql.User;
import com.geeknotes.web.service.exception.SqlEntityNotFoundException;
import com.geeknotes.web.util.EasyStopWatch;
import com.geeknotes.web.web.parsing.NotePayload;
import com.geeknotes.web.web.parsing.NotePayloadParser;
import com.geeknotes.web.web.parsing.SearchQuery;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.elasticsearch.core.FacetedPage;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class NotesServiceFacade {

    @Autowired
    private NotesElasticService notesElasticService;

    @Autowired
    private NotesSqlService notesSqlService;

    private final NotePayloadParser notePayloadParser = new NotePayloadParser();

    public EsNote create(String noteBody, User currentUser) {
        log.info("Creating new note");
        EasyStopWatch stopWatch = new EasyStopWatch().start();

        NotePayload notePayload = notePayloadParser.parse(noteBody);

        EsNote esNote = notesElasticService.create(notePayload, currentUser.getId());
        notesSqlService.create(notePayload, esNote.getId(), noteBody, currentUser);

        log.info("Note created in " + stopWatch.stop());
        return esNote;
    }

    public EsNote update(String esNoteId, String noteBody, User currentUser) {
        log.info("Updating note");
        EasyStopWatch stopWatch = new EasyStopWatch().start();

        NotePayload notePayload = notePayloadParser.parse(noteBody);

        EsNote esNote = notesElasticService.update(esNoteId, notePayload, currentUser.getId());
        notesSqlService.update(notePayload, esNote.getId(), noteBody, currentUser);

        log.info("Note has updated in " + stopWatch.stop());
        return esNote;
    }

    public FacetedPage<EsNote> search(SearchQuery searchQuery, int page, Long userId) {
        return notesElasticService.search(searchQuery, page, userId);
    }

    public Note findNoteOfUser(String esNoteId, Long userId) throws SqlEntityNotFoundException {
        return notesSqlService.findOneByEsNoteIdAndUserId(esNoteId, userId);
    }

    public String[] findDistinctTagsForUser(Long userId) {
        return notesSqlService.findDistinctTagsForUser(userId);
    }

    public void reindex() {
        int page = 0;
        final int PAGE_SIZE = 20;
        Page<Note> notes;

        notesElasticService.deleteAll();
        do {
            notes = notesSqlService.findAllAsPage(page, PAGE_SIZE);

            notes.getContent().stream().forEach(note -> {
                NotePayload payload = notePayloadParser.parse(note.getBody());
                notesElasticService.create(payload, note.getUserId(), note.getEsNoteId());
            });

            ++page;
        } while (notes.hasNext());
    }

    public void deleteByEsNoteId(String esNoteId) {
        notesSqlService.deleteNoteAndTagsByEsId(esNoteId);
        notesElasticService.deleteById(esNoteId);
    }
}
