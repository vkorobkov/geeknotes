package com.geeknotes.web.service.exception;

public class ESEntityNotFoundException extends RuntimeException {

    public ESEntityNotFoundException(Object searchByFieldName, Object searchByFieldValue, Class<?> entityType) {
        super("Entity of type " + entityType + " was not found by field: " + searchByFieldName + " with value: " +searchByFieldValue);
    }
}
