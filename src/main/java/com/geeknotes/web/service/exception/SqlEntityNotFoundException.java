package com.geeknotes.web.service.exception;

public class SqlEntityNotFoundException extends RuntimeException {

    public SqlEntityNotFoundException(Object searchByFieldName, Object searchByFieldValue, Class<?> entityType) {
        super("Entity of type " + entityType + " was not found by field: " + searchByFieldName + " with value: " +searchByFieldValue);
    }
}
