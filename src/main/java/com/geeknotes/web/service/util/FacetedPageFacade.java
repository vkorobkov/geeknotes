package com.geeknotes.web.service.util;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.springframework.data.domain.Page;

import java.util.List;

@Getter
@ToString
public class FacetedPageFacade<T> {

    private final List<T> content;
    private final PageInfo pageInfo;

    public FacetedPageFacade(Page<T> page, List<T> content) {
        this.content = content;
        this.pageInfo = new PageInfo(page);
    }

    @Getter
    @AllArgsConstructor
    private static class PageInfo {
        private final boolean isFirst;
        private final boolean isLast;
        private final long totalElements;
        private final int totalPages;
        private final int page;

        private PageInfo(Page<?> facetedPage) {
            this.isFirst = facetedPage.isFirst();
            this.isLast = facetedPage.isLast();
            this.totalElements = facetedPage.getTotalElements();
            this.totalPages = facetedPage.getTotalPages();
            this.page = facetedPage.getNumber();
        }
    }
}
