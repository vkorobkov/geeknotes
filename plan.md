
# The tracker
https://bitbucket.org/vkorobkov/geeknotes/issues

## To sort out urgently


## Important to me on release 1.0

* Buy domain
* Buy server
* Do corresponding re-brandings and renamings
* Create branch 1.0
* Configure server: install lxc-containers: elastic search, mysql, prod, jenkins
* Configure PROD deploy from jenkins (1.0)


## Configuration right after 1.0 deploy

* Configure CI within jenkins for master and 1.0
* Configure NGINX as a frontend proxy
* Configure monitoring tools for the containers
* Configure back up of database to S3


## Important for others on release 1.0

* Security: enable CSRF tokens in com.geeknotes.web.web.config.SecurityConfig
* New note template - for informative and links to markdown help and to our help
* Think of unauthorized access
* Write some stuff when user is not authorized
* Think of the welcome page
* Authorization page
* Some integration testing - create, edit, open and search.
* Write WIKI on bitbucket


## Ideas of self education

* JUnit + Mockito
* Spring Session http://docs.spring.io/spring-session/docs/current/reference/html5/
* New stuff from Spring 4.1 (http://www.slideshare.net/SpringCentral/s2gx2014-spring4webapps):
    * @RestController
    * @ControllerAdvice?
    * Correct generating of URLs for controllers (MvcUriComponentsBuilder)
    * Work with resources: caching, uglyfying, concatenating, fingerprints. Needs further investigation
    * Websockets support: SockJS, STOMP and so on. Needs further investigation
    * Controller tests with MockMvcConfigurer / MockMvcBuilder
* ReactJS ?


## Valuable things:

* Think of documentation/help pages
* Registration page and support
* Last accessed notes list
* Multi computer sync
* Print support
* Sharing of the notes
* Attaching files
* Uploading and attaching images
* Implement search in editor
* Implement search&replace in editor


## Things that would improve UX much:

* Handle 404 on loading note
* Create #last endpoint for loading last accessed document
* Ping server once a minute or so to keep the session alive
* Remember caret position when save/load note
* White theme
* Search results pagination
* Autocomplete tags in search input
* Show most popular tags in the search box before user hits "search"
* Duplicate line (CTRL+D)
* Move line up/down (ctrl-shift-arrow)
* Delete line (CTRL+Y)
* Hotkeys help
* Replace native alerts with beautiful


## The rest of the features:

* Status bar right after the editor - show document status, progress, errors and so on
* Generic 500 error handler
* Generic 404 error handler
* Generic 403 error handler
* Handle 500 on loading note
* Handle 403 on loading note
* Handle 500 on saving note
* Handle 404 on saving note
* Handle 403 on saving note
* Old notes collector(for future releases)
* Configurable new note template
* Configurable font size
* Download JS dependencies in JS-way somehow
* Some PhantomJS/FluentLenium testing
* Spring boot 1.2 http://spring.io/blog/2014/12/11/spring-boot-1-2-0-released
* Search popup - add "Search" button on the right of the query field
* Main page- load article popup
* Main page - loading JS/CSS modal until all the stuff is loaded
* Main page - global banner/message when any ajax is loading...
* Abbreviate result bodies on server
* IntelliJ results highlighting in large results bodies
* All notes explorer
* Create service page for the health status of ES, MySQL and all other external services
* Create alarm which is sending an email/sms when external services are down
* Create properties page(protected by superuser access)